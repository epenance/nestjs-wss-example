import { OnGatewayConnection, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';

@WebSocketGateway()
export class SocketGateway implements OnGatewayConnection{
  @WebSocketServer() server;

  constructor() {}

  afterInit() {
    console.log('Gateway is now running');
  }


  handleConnection(client, data) {
    console.log('Client connected');

    this.sendEvent('event', 'we connected');
  }

  sendEvent(event, data = {}) {
    this.server.emit(event, data);
  }

  sendEventToUser(userId, event, data = {}) {
    this.server.to(userId).emit(event, data);
  }
}