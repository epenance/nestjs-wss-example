import { Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SocketGateway } from './socket.gateway';
import { SocketModule } from './socket.module';

@Module({
  imports: [SocketModule],
  controllers: [AppController],
  providers: [AppService, SocketGateway],
})
export class AppModule implements NestModule {
  configure() {}
}
