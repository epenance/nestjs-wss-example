import { Get, Controller, Render } from '@nestjs/common';
import { AppService } from './app.service';
import { SocketGateway } from './socket.gateway';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private socket: SocketGateway,) {}

  @Get('/hello')
  hello() {
    console.log('Wow');

    this.socket.sendEvent('event', { event: 'hello', message: 'test'});
  }

  @Get()
  @Render('index')
  root() {
    return { message: 'Hello world' };
  }
}
